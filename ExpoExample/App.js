import React, { Component } from 'react';
import { SafeAreaView, Alert } from 'react-native';
import _ from 'lodash';
import {
  Skin03, Skin02, Skin01, Skin04, Skin05, Skin06, Skin07, Skin08
} from './src/components/NavBottomBar';
import { NavigationBar } from './appJson';

const SkinTemplate = {
  Skin03,
  Skin02,
  Skin01,
  Skin04,
  Skin05,
  Skin06,
  Skin07,
  Skin08
};

const widget = NavigationBar;

export default class App extends Component {
  constructor(props) {
    super(props);
    this.onChangeTab = this.onChangeTab.bind(this);
  }

  onChangeTab(key) {
    switch (key) {
      case 'tab1':
        Alert.alert('Note', key);
        break;
      case 'tab2':
        Alert.alert('Note', key);
        break;
      case 'tab3':
        Alert.alert('Note', key);
        break;
      case 'tab4':
        Alert.alert('Note', key);
        break;
      default:
        Alert.alert('Note', key);
        break;
    }
  }

  render() {
    const skin = _.get(widget, 'configs.skin', 'Skin01' );
    const Template = SkinTemplate[skin] || SkinTemplate['Skin01'];
    return (
      <SafeAreaView style={{ flex: 1, justifyContent: 'flex-end', backgroundColor: 'gray' }}>
        <Template
          widget={widget}
          onChangeTab={this.onChangeTab}
          tabs={[
          {
            key: 'tab1',
            title: 'Item 1',
            iconName: 'filter-1',
          },
          {
            key: 'tab2',
            title: 'Item 2',
            iconName: 'filter-2',
          },
          {
            key: 'home',
            title: 'Menu home',
            iconName: 'filter-3',
            type: 'main'
          },
          {
            key: 'tab3',
            title: 'Item 3',
            iconName: 'filter-3',
          },
          {
            key: 'tab4',
            title: 'Item 4',
            iconName: 'filter-4',
          },
        ]}
        />
      </SafeAreaView>
    );
  }
}