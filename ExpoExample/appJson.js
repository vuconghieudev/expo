const NavigationBar = {
  "id": "dwctidxlju",
  "alias": "NavigationBar",
  "configs": {
     "enable": true,
     "ui": {
        "bgColor": "primary",
        "textColor": "text",
        "activeColor": "#FFD95A",
        "notiColor": "#FFD95A",
        "showLabel": true,
        "showIcon": true,
        "showBadge": true
     },
     "screens": [],
     "skin": "Skin06"
  }
}

export {
   NavigationBar
};