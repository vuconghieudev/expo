import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import chroma from 'chroma-js';
import { LinearGradient } from 'expo';
import LabelButton from '../LabelButton';
import navStyles from '../navStyles';
import { Colors } from '../../../themes';

class Skin03 extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      indexTab: 0
    }
  }
  
  componentDidMount() {
    const { initialPage } = this.props;
    if(initialPage) {
      this.setState({
        indexTab: initialPage
      })
    }
  }

  onChangeTabIndex (index, key) {
    const { onChangeTab } = this.props;
    this.setState({ indexTab: index }, () => {
      if (typeof onChangeTab === 'function') {
        onChangeTab(key);
      }
    })
  }

  render() {
    const { indexTab } = this.state;
    const {
      colors,
      bgActiveColor,
      bgInactiveColor,
      tabs,
      showIcon,
      showLabel,
    } = this.props;
    return (
      <LinearGradient
        colors={colors}
        style={navStyles.container}
        locations={[1.0, 0.0]}
      >
        {tabs.map(((tab, index) => {
          const isActive = indexTab === index;
          const { iconName, title, key } = tab;
          return (
            <LabelButton
              key={index}
              label={title}
              containerStyle={{ margin: 5, borderRadius: 4 }}
              bgColor={isActive ? bgActiveColor : bgInactiveColor }
              activeColor='#333333'
              inactiveColor={chroma('#333333').alpha(0.5).css()}
              iconName={iconName}
              onPress={() => this.onChangeTabIndex(index, key)}
              isActive={isActive}
              showLabel={showLabel}
              showIcon={showIcon}
            />
          )
        }))}
      </LinearGradient>
    );
  }
}


Skin03.propTypes = {
  colors: PropTypes.array,
  tabs: PropTypes.array,
  bgActiveColor: PropTypes.string,
  bgInactiveColor: PropTypes.string,
  initialPage: PropTypes.number,
  onChangeTab: PropTypes.func,
  showIcon: PropTypes.bool,
  showLabel: PropTypes.bool,
}

Skin03.defaultProps = {
  colors: ['#F9A825', '#FFD95A'],
  tabs: [],
  bgActiveColor: 'rgba(255,255,255,0.5)',
  bgInactiveColor: 'transparent',
  initialPage: 0,
  onChangeTab: () => {},
  showIcon: true,
  showLabel: true,
}

export default Skin03;
