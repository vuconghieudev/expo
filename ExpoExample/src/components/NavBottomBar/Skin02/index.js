import React, { PureComponent } from 'react';
import { View } from 'react-native';
import PropTypes from 'prop-types';
import LabelButton from '../LabelButton';
import navStyles from '../navStyles';
import { Colors } from '../../../themes';

class Skin02 extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      indexTab: 0
    }
  }
  
  componentDidMount() {
    const { initialPage } = this.props;
    if(initialPage) {
      this.setState({
        indexTab: initialPage
      })
    }
  }

  onChangeTabIndex (index, key) {
    const { onChangeTab } = this.props;
    this.setState({ indexTab: index }, () => {
      if (typeof onChangeTab === 'function') {
        onChangeTab(key);
      }
    })
  }

  render() {
    const { indexTab } = this.state;
    const {
      bgActiveColor,
      bgInactiveColor,
      tabs,
      showIcon,
      showLabel,
    } = this.props;
    return (
      <View
        style={[navStyles.container, { borderBottomWidth: 2, borderBottomColor: bgActiveColor }]}
      >
        {tabs.map(((tab, index) => {
          const isActive = indexTab === index;
          const { iconName, title, key } = tab;
          return (
            <LabelButton
              key={index}
              label={title}
              textStyle={{
                fontWeight: 'bold'
              }}
              bgColor={isActive ? bgActiveColor : bgInactiveColor }
              activeColor='#FFFFFF'
              inactiveColor='rgba(255,255,255, 0.5)'
              iconName={iconName}
              onPress={() => this.onChangeTabIndex(index, key)}
              isActive={isActive}
              showLabel={showLabel}
              showIcon={showIcon}
            />
          )
        }))}
      </View>
    );
  }
}

Skin02.propTypes = {
  tabs: PropTypes.array,
  bgActiveColor: PropTypes.string,
  bgInactiveColor: PropTypes.string,
  initialPage: PropTypes.number,
  onChangeTab: PropTypes.func,
  showIcon: PropTypes.bool,
  showLabel: PropTypes.bool,
}

Skin02.defaultProps = {
  tabs: [],
  bgActiveColor: Colors.yellow1,
  bgInactiveColor: Colors.black2,
  initialPage: 0,
  onChangeTab: () => {},
  showIcon: true,
  showLabel: true,
}

export default Skin02;
