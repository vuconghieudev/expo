import React, { PureComponent } from 'react';
import { TouchableOpacity, View } from 'react-native';
import { Text } from 'native-base';
import PropTypes from 'prop-types';
import GradientCircleView from '../GradientCircleView';
import LabelButton from '../LabelButton';
import styles from './styles';
import navStyles from '../navStyles';
import { Colors, Metrics } from '../../../themes';

class Skin05 extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      indexTab: 0
    }
    this.onChangeTab = this.onChangeTab.bind(this);
  }

  onChangeTab(index, key){
    this.setState({
      indexTab: index
    }, () => {
      const { onChangeTab } = this.props;
      if (typeof onChangeTab === 'function') {
        onChangeTab(key);
      }
    })
  }

  render() {
    const {
      tabs
    } = this.props;
    const { indexTab } = this.state;
    return (
      <View
        style={[navStyles.container, { backgroundColor: '#fff'}]}
      >
        {tabs.map((tab, index) => {
          const { iconName, title, key, type } = tab;
          switch (type) {
            case 'main':
              return (
                <ImageButton
                  label="Menu item"
                  iconSize={Metrics.navBottomBar}
                  onPress={() => this.onChangeTab(index, key)}
                  iconName="photo-library"
                  iconColor="#FFF"
                  bgIconColors={['#9E9D24', '#D2CE56']}
                  inactiveColor="#BDBDBD"
                />
              );
            default:
              return (
                <LabelButton
                  key={key}
                  label={title}
                  onPress={() => this.onChangeTab(index, key)}
                  isActive={indexTab === index}
                  activeColor="#9E9D24"
                  inactiveColor="#BDBDBD"
                  iconName={iconName}
                  showLabel={false}
                  showIcon
                />
              );
          }
        })}
      </View>
    );
  }
}


const ImageButton = ({
  onPress,
  iconName,
  iconSize,
  iconColor,
  bgIconColors,
}) => {
  const borderWidth = 5;
  return (
    <TouchableOpacity
      activeOpacity={0.8}
      onPress={onPress}
      style={{
        flex: 1,
        alignItems: 'center',
    }}
    >
      <GradientCircleView
        size={iconSize}
        borderWidth={borderWidth}
        borderColor="#FFF"
        iconName={iconName}
        iconColor={iconColor}
        bgColors={bgIconColors}
        containerStyle = {{ position: 'absolute', bottom: 4 }}
      />
    </TouchableOpacity>
  );
}

Skin05.propTypes = {
  tabs: PropTypes.array,
  onChangeTab: PropTypes.func
}

Skin05.defaultProps = {
  tabs: [],
  onChangeTab: () => {}
}

export default Skin05;
