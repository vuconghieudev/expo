import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { LinearGradient } from 'expo';
import LabelButton from '../LabelButton';
import styles from './styles';
import navStyles from '../navStyles';
import { Colors } from '../../../themes';

class Skin01 extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      indexTab: 0
    }
  }
  
  componentDidMount() {
    const { initialPage } = this.props;
    if(initialPage) {
      this.setState({
        indexTab: initialPage
      })
    }
  }

  onChangeTabIndex (index, key) {
    const { onChangeTab } = this.props;
    this.setState({ indexTab: index }, () => {
      if (typeof onChangeTab === 'function') {
        onChangeTab(key);
      }
    })
  }

  render() {
    const { indexTab } = this.state;
    const {
      colors,
      underLineColor,
      tabs = [],
      showIcon,
      showLabel,
      showBadge = true
    } = this.props;
    return (
      <LinearGradient
        colors={colors}
        style={navStyles.container}
        locations={[1.0, 0.0]}
      >
        {tabs.map(((tab, index) => {
          const isActive = indexTab === index;
          const { iconName, title, key, badge } = tab;
          return (
            <LabelButton
              key={index}
              label={title}
              activeColor='#FFFFFF'
              inactiveColor='rgba(255,255,255, 0.5)'
              iconName={iconName}
              badge={badge}
              showBadge={showBadge && badge && badge !== 0}
              onPress={() => this.onChangeTabIndex(index, key)}
              isActive={isActive}
              showLabel={showLabel}
              showIcon={showIcon}
              containerStyle={{
                borderBottomWidth: 2, borderBottomColor: isActive ? underLineColor : 'transparent'
              }}
            />
          )
        }))}
      </LinearGradient>
    );
  }
}

Skin01.defaultProps = {
  tabs: [],
  colors: [Colors.green1, Colors.green2],
  underLineColor: Colors.white,
  showIcon: true,
  showLabel:  true,
  initialPage: 0,
  onChangeTab: () => {}
}

Skin01.propTypes = {
  tabs: PropTypes.array,
  colors: PropTypes.array,
  underLineColor: PropTypes.string,
  // badgeColor: PropTypes.string,
  showIcon: PropTypes.string,
  showLabel:  PropTypes.string,
  initialPage: PropTypes.number,
  onChangeTab: PropTypes.func
}

export default Skin01;
