import { StyleSheet } from 'react-native';
import { Metrics } from '../../themes';
const navStyles = StyleSheet.create({
  container: {
    height: Metrics.navBottomBar,
    flexDirection: 'row',
    // paddingVertical: 15
  }
});

export default navStyles;