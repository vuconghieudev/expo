import Skin01 from './Skin01';
import Skin02 from './Skin02';
import Skin03 from './Skin03';
import Skin04 from './Skin04';
import Skin05 from './Skin05';
import Skin06 from './Skin06';
import Skin07 from './Skin07';
import Skin08 from './Skin08';

export {
  Skin01,
  Skin02,
  Skin03,
  Skin04,
  Skin05,
  Skin06,
  Skin07,
  Skin08
};
