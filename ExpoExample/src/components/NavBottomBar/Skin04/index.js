import React, { PureComponent } from 'react';
import { TouchableOpacity, View } from 'react-native';
import PropTypes from 'prop-types';
import { Text } from 'native-base';
import chroma from 'chroma-js';
import Icon from 'react-native-vector-icons/MaterialIcons';
import navStyles from '../navStyles';
import { Colors } from '../../../themes';

const Dot = ({ size, bgColor, containerStyle }) => {
  return (
    <View style={[{
      backgroundColor: bgColor,
      width: size,
      height: size,
      borderRadius: size,
    },containerStyle]}
    />
  );
};

class Skin04 extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      indexTab: 0
    }
  }
  
  componentDidMount() {
    const { initialPage } = this.props;
    if(initialPage) {
      this.setState({
        indexTab: initialPage
      })
    }
  }

  onChangeTabIndex (index, key) {
    const { onChangeTab } = this.props;
    this.setState({ indexTab: index }, () => {
      if (typeof onChangeTab === 'function') {
        onChangeTab(key);
      }
    })
  }

  render() {
    const { indexTab } = this.state;
    const {
      iconSize,
      textSize,
      bgColor,
      activeColor,
      inactiveColor,
      dotColor,
      tabs
    } = this.props;
    return (
      <View
        style={[navStyles.container]}
      >
        {tabs.map(((tab, index) => {
          const isActive = indexTab === index;
          const { iconName, title, key, badge } = tab;
          return (
            <TouchableOpacity
              key={index}
              activeOpacity={0.8}
              style={{
                flex: 1,
                backgroundColor: bgColor,
                justifyContent: 'center',
                alignItems: 'center',
              }}
              onPress={() => this.onChangeTabIndex(index, key)}
            >
              {iconName ?
              <View style={[badge && !isActive ? { paddingTop: 5, paddingRight: 5 } : {}]}>
                <Icon
                  name={iconName}
                  size={isActive ? iconSize * 1.2 : iconSize}
                  color={isActive ? activeColor : inactiveColor}/>
                {!isActive && badge ?
                  <Dot
                    size={8}
                    bgColor={dotColor}
                    containerStyle={{ position: 'absolute', right: 0 }}
                /> : <View />}
              </View> : <View />}
              {isActive ? <View style={{ marginTop: 2, paddingHorizontal: 4 }}>
                <Text
                  numberOfLines={1}
                  style={{
                    fontSize: textSize,
                    textAlign: 'center',
                    color: isActive ? activeColor : inactiveColor
                  }}>{title}</Text>
              </View> : <View />}
            </TouchableOpacity>
          )
        }))}
      </View>
    );
  }
}

Skin04.propTypes = {
  tabs: PropTypes.array,
  onChangeTab: PropTypes.func,
  iconSize: PropTypes.number,
  textSize: PropTypes.number,
  bgColor: PropTypes.string,
  activeColor: PropTypes.string,
  inactiveColor: PropTypes.string,
  dotColor: PropTypes.string
}

Skin04.defaultProps = {
  tabs: [],
  onChangeTab: () => {},
  iconSize: 20,
  textSize: 12,
  opacity: 0.5,
  bgColor: Colors.black2,
  activeColor: Colors.yellow1,
  inactiveColor: chroma('#FFFFFF').alpha(0.5).css(),
  dotColor: '#F9A825'
}

export default Skin04;
