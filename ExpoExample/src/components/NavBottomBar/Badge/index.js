import React, { Component } from 'react';
import {
  Text,
  View,
} from 'react-native';
import PropTypes from 'prop-types';
import styles from './styles';

const MAX_BADGE = 99;

class Badge extends Component {
  constructor(props) {
    super(props);
    this.defaultTextStyle = {
      fontSize: 10,
      color: '#FFF',
      fontWeight: 'bold'
    };
    this.textStyle = { ...this.defaultTextStyle, ...props.textStyle };
  }

  render() {
    const {
      containerStyle,
      showBadge,
      bgColor,
      numberBadge
    } = this.props;
    const badge = numberBadge > MAX_BADGE ? `${MAX_BADGE}+` : numberBadge;
    const width = (String(badge).length * 6 + 10);
    if (!showBadge) return (<View />);
    return (
      <View
        style={[
          styles.container,
          containerStyle,
          {
            width: width,
            backgroundColor: bgColor,
            borderRadius: width
          },
        ]}
      >
        <Text
          style={{
            color: this.textStyle.color,
            fontSize: this.textStyle.fontSize,
            fontWeight: this.textStyle.fontWeight,
            margin: 2
          }}
        >
          {badge}
        </Text>
      </View>
    );
  }
}

Badge.defaultProps = {
  showBadge: true,
  numberBadge: 0,
  bgColor: '#F9A825'
}

Badge.propTypes = {
  numberBadge: PropTypes.number,
  showBadge: PropTypes.bool
}

export default Badge;
