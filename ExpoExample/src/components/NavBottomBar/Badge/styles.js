import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  container: {
    overflow: 'hidden',
    justifyContent: 'center',
    alignItems: 'center',
  },
  containerStyle: {
    borderRadius: 10,
    minWidth: 18,
    height: 18,
  },
});
