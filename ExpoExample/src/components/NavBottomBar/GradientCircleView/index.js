import React, { PureComponent } from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import PropTypes from 'prop-types';
import { LinearGradient } from 'expo';
import { Icon } from 'native-base';

class GradientCircleView extends PureComponent {
  render() {
    const {
      bgColors,
      size,
      iconName,
      iconColor,
      borderWidth,
      borderColor,
      containerStyle
    } = this.props;
    const viewSize = size + borderWidth;
    return (
      <LinearGradient
        colors={bgColors}
        locations={[1.0, 0.0]}
        style={[{
          width: viewSize,
          height: viewSize,
          borderRadius: viewSize * 0.5,
          justifyContent: 'center',
          alignItems: 'center',
          borderWidth: borderWidth,
          borderColor: borderColor,
        }, containerStyle]}
      >
        <Icon type="MaterialIcons" name={iconName}  style={{ color: iconColor, fontSize: size / 3 }} />
      </LinearGradient>
    )
  }
}

GradientCircleView.defaultProps = {
  bgColors: ['#F9A825', '#FFD95A'],
  borderWidth: 0,
  borderColor: '#FFF',
  size: 64,
  iconName: "photo-library",
  iconColor: "#FFF",
  onPress: () => {}
}

GradientCircleView.propTypes = {
  bgColors: PropTypes.array,
  borderWidth: PropTypes.number,
  borderColor: PropTypes.string,
  size: PropTypes.number,
  onPress: PropTypes.func,
  iconName: PropTypes.string,
  iconColor: PropTypes.string
}

export default GradientCircleView;
