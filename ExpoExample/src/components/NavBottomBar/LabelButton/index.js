import React, { PureComponent } from 'react';
import { TouchableOpacity, View } from 'react-native';
import PropTypes from 'prop-types';
import { Text, Icon } from 'native-base';
import Badge from '../Badge';
import styles from './styles';

class LabelButton extends PureComponent {
  constructor(props) {
    super(props);
    this.defaultTextStyle = {
      fontSize: 12,
      fontWeight: 'normal'
    };
    this.textStyle = { ...this.defaultTextStyle, ...props.textStyle };
  }

  render() {
    const {
      label,
      badge,
      bgColor,
      activeColor,
      inactiveColor,
      iconName,
      onPress,
      isActive,
      showLabel,
      showIcon,
      showBadge,
      iconSize,
      containerStyle,
    } = this.props;
    return (
      <TouchableOpacity
        activeOpacity={0.8}
        style={[{
          backgroundColor: bgColor,
        }, styles.container, containerStyle]}
        onPress={onPress}
      >
          {showIcon ?
          <View>
            <Icon
              type="MaterialIcons"
              name={iconName}
              style={{ fontSize: iconSize, color: isActive ? activeColor : inactiveColor }}
            />
            <Badge
              numberBadge={badge}
              showBadge={showBadge}
              containerStyle={{ position: 'absolute', top: -10, right: -6 }}
            />
          </View>
          : <View />}
          {showLabel ? <View style={{ marginTop: 2, paddingHorizontal: 4 }}>
            <Text
              numberOfLines={1}
              ellipsizeMode="tail"
              style={{
                fontSize: this.textStyle.fontSize,
                fontWeight: this.textStyle.fontWeight,
                textAlign: 'center',
                color: isActive ? activeColor : inactiveColor
              }}>{label}</Text>
          </View> : <View />}
      </TouchableOpacity>
    );
  }
}

LabelButton.propTypes = {
  label: PropTypes.string,
  bgColor: PropTypes.string,
  activeColor: PropTypes.string,
  inactiveColor: PropTypes.string,
  iconName: PropTypes.string,
  onPress: PropTypes.func,
  isActive: PropTypes.bool,
  showLabel: PropTypes.bool,
  showIcon: PropTypes.bool,
  iconSize: PropTypes.number,
  containerStyle: PropTypes.object,
  badge: PropTypes.number,
  showBadge: PropTypes.bool
}

LabelButton.defaultProps = {
  onPress: () => {},
  isActive: false,
  showLabel: false,
  showIcon: false,
  iconSize: 20,
  bgColor: "transparent",
  showBadge: false
}

export default LabelButton;
