import React, { PureComponent } from 'react';
import { TouchableOpacity, View } from 'react-native';
import { Text } from 'native-base';
import PropTypes from 'prop-types';
import GradientCircleView from '../GradientCircleView';
import LabelButton from '../LabelButton';
import styles from './styles';
import navStyles from '../navStyles';
import { Colors, Metrics } from '../../../themes';

class Skin05 extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      indexTab: 0
    }
    this.onChangeTab = this.onChangeTab.bind(this);
  }

  onChangeTab(index, key){
    this.setState({
      indexTab: index
    }, () => {
      const { onChangeTab } = this.props;
      if (typeof onChangeTab === 'function') {
        onChangeTab(key);
      }
    })
  }

  render() {
    const {
      tabs
    } = this.props;
    const { indexTab } = this.state;
    return (
      <View
        style={[navStyles.container, { backgroundColor: '#fff'}]}
      >
        {tabs.map((tab, index) => {
          const { iconName, title, key, type } = tab;
          switch (type) {
            case 'main':
              return (
                <ImageButton
                  label="Menu item"
                  iconSize={Metrics.navBottomBar}
                  onPress={() => this.onChangeTab(index, key)}
                  isActive={indexTab === index}
                  iconName="photo-library"
                  iconColor="#FFF"
                  bgIconColors={['#F9A825', '#FFD95A']}
                  activeColor="#F9A825"
                  inactiveColor="#BDBDBD"
                />
              );
            default:
              return (
                <LabelButton
                  key={key}
                  label={title}
                  onPress={() => this.onChangeTab(index, key)}
                  isActive={indexTab === index}
                  activeColor="#F9A825"
                  inactiveColor="#BDBDBD"
                  iconName={iconName}
                  showLabel
                  showIcon
                />
              );
          }
        })}
      </View>
    );
  }
}


const ImageButton = ({
  onPress,
  iconName,
  iconSize,
  iconColor,
  bgIconColors,
  activeColor,
  inactiveColor,
  isActive = false,
  label,
  showLabel = true
}) => {
  return (
    <TouchableOpacity
      activeOpacity={0.9}
      onPress={onPress}
      style={{
        flex: 1, bottom: Metrics.navBottomBar * 0.5,
        alignItems: 'center',
    }}
    >
      <GradientCircleView
        size={iconSize}
        iconName={iconName}
        iconColor={iconColor}
        bgColors={bgIconColors}
      />
      {showLabel ? <View style={{ marginTop: 4 }}>
        <Text
          numberOfLines={1}
          ellipsizeMode="tail"
          style={{
            fontSize: 12,
            fontWeight: "normal",
            textAlign: 'center',
            color: isActive ? activeColor : inactiveColor
          }}
        >{label}</Text>
      </View> : <View />}
    </TouchableOpacity>
  );
}

Skin05.propTypes = {
  tabs: PropTypes.array,
  onChangeTab: PropTypes.func
}

Skin05.defaultProps = {
  tabs: [],
  onChangeTab: () => {}
}

export default Skin05;
