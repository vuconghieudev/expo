import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import Feather from 'react-native-vector-icons/Feather';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Entypo from 'react-native-vector-icons/Entypo';
import EvilIcons from 'react-native-vector-icons/EvilIcons';
import Ionicons from 'react-native-vector-icons/Ionicons';
import Octicons from 'react-native-vector-icons/Octicons';
import Foundation from 'react-native-vector-icons/Foundation';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import IconSimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';
import AntDesign from 'react-native-vector-icons/AntDesign';

import { normalize } from '../../utils/AppValues';

class Icons extends PureComponent {
  render() {
    const {
      typeIcon, icon, color, size, style
    } = this.props;
    switch (typeIcon) {
      case 'MaterialIcons':
        return <MaterialIcons name={icon} color={color} size={size} style={style} />;
      case 'MaterialCommunityIcons':
        return <MaterialCommunityIcons name={icon} color={color} size={size} style={style} />;
      case 'Feather':
        return <Feather name={icon} color={color} size={size} style={style} />;
      case 'Ionicons':
        return <Ionicons name={icon} color={color} size={size} style={style} />;
      case 'Octicons':
        return <Octicons name={icon} color={color} size={size} style={style} />;
      case 'Foundation':
        return <Foundation name={icon} color={color} size={size} style={style} />;
      case 'SimpleLineIcons':
        return <IconSimpleLineIcons name={icon} color={color} size={size} style={style} />;
      case 'EvilIcons':
        return <EvilIcons name={icon} color={color} size={size} style={style} />;
      case 'AntDesign':
        return <AntDesign name={icon} color={color} size={size} style={style} />;
      case 'Entypo':
        return <Entypo name={icon} color={color} size={size} style={style} />;
      case 'FontAwesome':
        return <FontAwesome name={icon} color={color} size={size} style={style} />;
      default:
        return <FontAwesome5 name={icon} color={color} size={size} style={style} />;
    }
  }
}

Icons.defaultProps = {
  icon: '',
  color: 'black',
  size: 20
};

Icons.propTypes = {
  icon: PropTypes.string,
  color: PropTypes.string,
  size: PropTypes.number,
  typeIcon: PropTypes.string
};

export default Icons;
