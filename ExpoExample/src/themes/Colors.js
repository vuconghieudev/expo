export default Colors = {
  yellow1: '#F9A825',
  yellow2: '#FFD95A',
  black: '#000000',
  black2: '#333333',
  white: '#FFFFFF',
  gray1: '#828282',
  green1: '#00695C',
  green2: '#439889',
  silver: '#BDBDBD',
}