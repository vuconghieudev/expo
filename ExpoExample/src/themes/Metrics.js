import { Platform  } from 'react-native';

const metrics = {
  navBottomBar: Platform.OS === 'ios' ? 64 : 53,
  borderRadiusButton: 4,
  icon: {
    tiny: 15,
    small: 20,
    medium: 30,
    large: 40
  }
}

export default metrics;